//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Medicare.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Appointment
    {
        public string AppointID { get; set; }
        public string PatientID { get; set; }
        public string DoctorID { get; set; }
        public string problem { get; set; }
        public Nullable<System.DateTime> appointmentTimeDate { get; set; }
    }
}
